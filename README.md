# Humane-Dark Theme

## About

Humane-Dark color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-humanedark_code.png](./images/sjsepan-humanedark_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-humanedark_codium.png](./images/sjsepan-humanedark_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-humanedark_codedev.png](./images/sjsepan-humanedark_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-humanedark_ads.png](./images/sjsepan-humanedark_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-humanedark_theia.png](./images/sjsepan-humanedark_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-humanedark_positron.png](./images/sjsepan-humanedark_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/9/2025
