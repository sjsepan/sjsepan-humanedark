# Humane-Dark Color Theme - Change Log

## [0.2.5]

- tweak widget/notification borders and backgrounds

## [0.2.4]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.2.3]

- update readme and screenshot

## [0.2.2]

- fix titlebar border, FG/BG in custom mode

## [0.2.1]

- fix manifest and pub WF

## [0.2.0]

- dimmer button BG so as to not compete visually with the project names in the Git sidebar
- fix manifest repo links
- retain v0.1.2 for those who prefer the earlier style

## [0.1.2]

- fix fold indicators: editorGutter.foldingControlForeground: lighten indicators
- scroll slider contrast:
fix scrollbar, minimap slider transparencies (dark):
"minimapSlider.activeBackground": "#ffffff40",
"minimapSlider.background": "#ffffff20",
"minimapSlider.hoverBackground": "#ffffff60",
"scrollbarSlider.activeBackground": "#ffffff40",
"scrollbarSlider.background": "#ffffff20",
"scrollbarSlider.hoverBackground": "#ffffff60",
- completion list contrast:
editorSuggestWidget.selectedBackground darkened
editorSuggestWidget.background: lightened
list.hoverBackground OK

## [0.1.1]

- fix badges
- fix editor split border
- fix tab hover background
- adjust hover shadows/borders
- dim borders
- fix readme format

## [0.1.0]

- dim panel title inactive foreground
- retain v0.0.6 for those who prefer original style
- swap some panel BG colors, light for dark
- adjust FG / BG contrasts on various elements
- use lighter borders

## [0.0.6]

- fix light/dark category with "uiTheme": "vs-dark" in package.json

## [0.0.5]

- hover widget FG/BG

## [0.0.4]

- notification FG/BG
- darken find result BG

## [0.0.3]

- swap active/inactive tab background, highlight active tab border
- darken border
- Fix contrasts in input selections, tabs

## [0.0.2]

- Fix contrasts in editor widgets (command list, find) and in sidebar

## [0.0.1]

- Initial release
